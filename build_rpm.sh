#!/bin/bash

# Define RPMs to be downloaded
declare -a arr=(
  "https://gitlab.com/harbottle/omnibus-bolt/-/jobs/artifacts/master/raw/packages/centos7/bolt-0.19.1-1.el7.x86_64.rpm?job=publish%3Aartifacts"
  "https://gitlab.com/harbottle/centos7-elastalert/-/jobs/46670646/artifacts/raw/elastalert-0.1.25-3.el7.x86_64.rpm"
)

mkdir -p public/7/x86_64

echo "Downloading RPMs..."
for i in "${arr[@]}"
do
   wget --directory-prefix=public/7/x86_64 --trust-server-names "$i"
done

len=${#arr[@]}

# Build release RPM
echo "Building release RPM..."
rpmbuild --quiet --define "_topdir $PWD/release" --ba release/SPECS/*.spec
mv release/RPMS/*/*.rpm $PWD
ln -s *release*.rpm $CI_PROJECT_NAME-release.rpm
mv *release*.rpm  public/7/x86_64/

# Sign RPM files
echo -e "%_signature gpg" >> $HOME/.rpmmacros
echo -e "%_gpg_path /root/.gnupg" >> $HOME/.rpmmacros
echo -e "%_gpg_name $GPG_NAME" >> $HOME/.rpmmacros
echo -e "%_gpgbin /usr/bin/gpg" >> $HOME/.rpmmacros
echo -e "%_gpg_digest_algo sha256" >> $HOME/.rpmmacros

echo "$GPG_PUBLIC_KEY" > /tmp/public
echo "$GPG_PRIVATE_KEY" > /tmp/private
gpg --allow-secret-key-import --import /tmp/private
rpm --import /tmp/public

for f in public/7/x86_64/*.rpm
do
  echo "GPG signing $f..."
expect <(cat <<EOD
spawn rpm --resign $f
expect -exact "Enter pass phrase: "
send -- "$GPG_PASS_PHRASE\r"
expect eof
EOD
)
done

# Create yum repo
createrepo public/7/x86_64
