# Omnibus packages for Enterprise Linux

**[Browse the yum repo.](https://harbottle.gitlab.io/omnibus/7/x86_64)**

A yum repo of RPM files containing [Omnibus](https://github.com/chef/omnibus)
packages and other packages where all dependencies are bundled. The packages are
suitable for CentOS 7 (and RHEL, Oracle Linux, etc.).

Packages have been built using [Omnibus](https://github.com/chef/omnibus) or
[fpm](https://github.com/jordansissel/fpm) and
[GitLab CI](https://about.gitlab.com/gitlab-ci/). The yum repo is hosted
courtesy of [GitLab Pages](https://pages.gitlab.io/).

## Quick Start

```bash
# Install the repo
sudo yum -y install https://harbottle.gitlab.io/omnibus/7/x86_64/omnibus-release.rpm

# Install a package, e.g.:
sudo yum -y install bolt
```

## Package sources

- [bolt](https://gitlab.com/harbottle/omnibus-bolt)
- [elastalert](https://gitlab.com/harbottle/centos7-elastalert)

## Removed packages

- [opentaxii](https://gitlab.com/harbottle/centos7-opentaxii/) (moved to
[harbottle-main](https://harbottle.gitlab.io/harbottle-main))
