FROM centos:latest
MAINTAINER harbottle <grainger@gmail.com>
RUN yum -y install https://harbottle.gitlab.io/harbottle-main/7/x86_64/harbottle-main-release.rpm
RUN yum -y install epel-release
RUN yum -y install ergel-release
RUN yum -y install rubygem-filesize
RUN yum -y install nodejs-bower
RUN yum -y install wget
RUN yum -y install rpm-build
RUN yum -y install yum-utils
RUN yum -y install rpmdevtools
RUN yum -y install expect
RUN yum -y install createrepo
RUN yum -y install rpm-sign
